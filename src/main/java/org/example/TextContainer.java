package org.example;

import java.io.FileWriter;
import java.io.IOException;

@SaveTo(path = "c:\\1\\2.txt")
public class TextContainer {
    private String test ="New text";
    public TextContainer(String test) {
        this.test = test;
    }

    @Saver
    public void Save(String FileName){
        try (FileWriter fw = new FileWriter(FileName)) {
            fw.write(test);
            System.out.println("File "+FileName+" saved");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
