package org.example;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class TextSaver {
    private Object obj;
    public void Save(){
        Class<?> cmd = obj.getClass();
        SaveTo annotation = cmd.getAnnotation(SaveTo.class);
        if (annotation != null) {

            for (Method m : cmd.getMethods() ){
                if (m.isAnnotationPresent(Saver.class)){

                    try {
                        m.invoke(obj, annotation.path());
                    } catch (IllegalAccessException e) {
                        throw new RuntimeException(e);
                    } catch (InvocationTargetException e) {
                        throw new RuntimeException(e);
                    }
                }

            }
        }
    }

    public TextSaver(Object obj) {
        this.obj = obj;
    }

    public void setObj() {

    }
    public void setObj(Object obj) {
        this.obj = obj;
    }

    public Object getObj() {
        return obj;
    }

    @Override
    public String toString() {
        return "TextSaver{" +
                "obj=" + obj +
                '}';
    }
}
